/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.javatestproject;

import java.time.LocalDate;

/**
 *
 * @author aomza
 */
public class JobService {
    public static boolean checkEnableTime(LocalDate startTime, LocalDate endTime,LocalDate today){
        if(today.isBefore(startTime)){
            return false;
        }
        if(today.isAfter(endTime)){
            return false;
        }
        if(today.isEqual(startTime)){
            return true;
        }
        if(today.isEqual(endTime)){
            return true;
        }
        return true;
    }
}
