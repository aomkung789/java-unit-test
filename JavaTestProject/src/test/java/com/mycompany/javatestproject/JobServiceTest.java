/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.javatestproject;

import java.time.LocalDate;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author aomza
 */
public class JobServiceTest {
    
    public JobServiceTest() {
    }

    @org.junit.jupiter.api.BeforeAll
    public static void setUpClass() throws Exception {
    }

    @org.junit.jupiter.api.AfterAll
    public static void tearDownClass() throws Exception {
    }

    @org.junit.jupiter.api.BeforeEach
    public void setUp() throws Exception {
    }

    @org.junit.jupiter.api.AfterEach
    public void tearDown() throws Exception {
    }

    /**
     * Test of checkEnableTime method, of class JobService.
     */
    @org.junit.jupiter.api.Test
    public void testCheckEnableTimeTodayIsBtweenStarttTimeAndEndTime() {
        System.out.println("checkEnableTime");
        // Arrange
        LocalDate startTime = LocalDate.of(2021,1,31);
        LocalDate endTime = LocalDate.of(2021,2,05);
        LocalDate today = LocalDate.of(2021,2,03);
        boolean expResult = true;
        // Act
        boolean result = JobService.checkEnableTime(startTime, endTime, today);
        
       // Assert
        assertEquals(expResult, result);
    }
    
     public void testCheckEnableTimeTodayIsBeforeStartTime() {
        System.out.println("checkEnableTime");
        // Arrange
        LocalDate startTime = LocalDate.of(2021,1,31);
        LocalDate endTime = LocalDate.of(2021,2,05);
        LocalDate today = LocalDate.of(2021,1,30);
        boolean expResult = false;
        // Act
        boolean result = JobService.checkEnableTime(startTime, endTime, today);
        
       // Assert
        assertEquals(expResult, result);
    }
     
     public void testCheckEnableTimeTodayIsAfterEndTime() {
        System.out.println("checkEnableTime");
        // Arrange
        LocalDate startTime = LocalDate.of(2021,1,31);
        LocalDate endTime = LocalDate.of(2021,2,05);
        LocalDate today = LocalDate.of(2021,2,06);
        boolean expResult = false;
        // Act
        boolean result = JobService.checkEnableTime(startTime, endTime, today);
        
       // Assert
        assertEquals(expResult, result);
    }
     
     public void testCheckEnableTimeTodayIsEqualStartTime() {
        System.out.println("checkEnableTime");
        // Arrange
        LocalDate startTime = LocalDate.of(2021,1,31);
        LocalDate endTime = LocalDate.of(2021,2,05);
        LocalDate today = LocalDate.of(2021,1,31);
        boolean expResult = true;
        // Act
        boolean result = JobService.checkEnableTime(startTime, endTime, today);
        
       // Assert
        assertEquals(expResult, result);
    }
     
     public void testCheckEnableTimeTodayIsEqualEndTime() {
        System.out.println("checkEnableTime");
        // Arrange
        LocalDate startTime = LocalDate.of(2021,1,31);
        LocalDate endTime = LocalDate.of(2021,2,05);
        LocalDate today = LocalDate.of(2021,2,05);
        boolean expResult = true;
        // Act
        boolean result = JobService.checkEnableTime(startTime, endTime, today);
        
       // Assert
        assertEquals(expResult, result);
    }
    
}
